//Mi primer programa java

public class HolaMundo {
   public static void main (String args []) {
	   
	   /***********************************/
	   /*         Type of variable        */
	   /***********************************/
	   
	   String greet  = "Hello World across Java";
	   var    grt    = "Second Greet"           ; 
	   var    num    = 12                       ; 
	   
	   /***********************************/
	   /*        variable of conca        */
	   /***********************************/
	   
	   var    name = "Juan"  ; 
	   var    lsna = "Hola"  ;
	   var    num2 = 3       ;
	   
		
	   
	   System.out.println(greet)                     ;
	   System.out.println(num)                       ; //print de numeros
	   System.out.println(grt)                       ; 
	   System.out.println(name + " "+ lsna)          ; //concatenacion
	   System.out.println(num + num2 + " "+ greet)   ; //print de suma de numeros mas palabras 
	   System.out.println(lsna + " " + num + num2)   ; //no suma numero, reconoce primero una cadena
	   System.out.println("Nueva Linea: \t" + greet) ; //impresion con salto de linea \n, tab \t, retroceso \b, comilla simple \' o  doble \"
	   System.out.println("doble \"" + greet +"\"")  ; // cerrar comillas 
   }
   
}

